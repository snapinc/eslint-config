module.exports = {
    // extends: 'google',
    plugins: [
        'html',
    ],
    parserOptions: {
        // ecmaVersion: 7,
        sourceType: 'module',
        // sourceType: 'script',
        // ecmaFeatures: {
        //     jsx: true,
        //     experimentalObjectRestSpread: true
        // }
    },
    env: {
        node: true,
        browser: true,
    },
    globals: {
    //   snap: true,
    },
    rules: {
        'indent': [2, 4],
        'comma-dangle': [2, 'always-multiline'],
        'space-before-function-paren': [2, 'never'],
        'valid-jsdoc': [2, {
            requireReturn: false,
            prefer: {
                returns: 'return',
            },
        }],
        'require-jsdoc': 1,
        'no-warning-comments': [0],
        'max-len': [1, 120, 4, {
            ignoreComments: true,
            ignoreUrls: true,
        }],
        'camelcase': [0],
        'object-curly-spacing': 0,
        'arrow-parens': [0],
        'padded-blocks': [0],
    },
};
